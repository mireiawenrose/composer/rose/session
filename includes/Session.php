<?php
declare(strict_types = 1);

namespace Rose\Framework\Session;

use Mireiawen\Input\AbstractInput;
use Mireiawen\Input\MissingValue;
use Rose\Framework\Core;
use Rose\Framework\ModuleInterface;
use RuntimeException;
use function is_null;

/**
 * Basic session creation, validation and invalidation
 *
 * @copyright 2013-2023 Mira "Mireiawen" Manninen
 * @package Mireiawen\RoseFramework
 */
class Session extends AbstractInput implements ModuleInterface
{
	/**
	 * The name for the module
	 *
	 * @var string
	 */
	public const string MODULE_NAME = 'Session';
	
	/**
	 * Key for session expiry information
	 *
	 * @var string
	 */
	protected const string EXPIRY_KEY = 'Expires';
	
	/**
	 * Key for obsolete session information
	 *
	 * @var string
	 */
	protected const string OBSOLETE_KEY = 'Obsolete';
	
	/**
	 * Key for server generated session ID information
	 *
	 * @var string
	 */
	protected const string SID_KEY = 'Server Generated SID';
	
	/**
	 * Key for remote address information
	 *
	 * @var string
	 */
	protected const string REMOTE_ADDRESS_KEY = 'Remote Address';
	
	/**
	 * Key for user agent information
	 *
	 * @var string
	 */
	protected const string USER_AGENT_KEY = 'User Agent';
	
	/**
	 * The framework core
	 *
	 * @var Core
	 */
	protected Core $core;
	
	/**
	 * The session timeout
	 *
	 * @var int
	 */
	protected int $timeout = 0;
	
	/**
	 * The obsolete session timeout
	 *
	 * @var int
	 */
	protected int $obsolete_timeout = 0;
	
	/**
	 * Set up the session basics
	 *
	 * @param Core $core
	 *    The framework core
	 */
	public function __construct(Core $core)
	{
		// Register the core
		$this->core = $core;
		$this->core->Register($this);
		
		// Session won't work on CLI anyway
		if (PHP_SAPI === 'cli')
		{
			return;
		}
		
		// Make sure the timeouts are defined
		$this->timeout = $this->core->Configuration()->GetInt('session_timeout', 3600);
		$this->obsolete_timeout = $this->core->Configuration()->GetInt('session_timeout_obsolete', 30);
		
		// Set the session cookie
		session_name(sprintf('%s%s', $this->core->Configuration()->GetString('session_prefix', 'session_'), $this->core->Configuration()->GetString('session_identifier', 'RoseFramework')));
		
		// Get the session variables from the configuration
		$domain = $this->core->Configuration()->GetString('session_domain', '');
		$path = $this->core->Configuration()->GetString('session_path', '/');
		$limit = $this->core->Configuration()->GetInt('session_lifetime', 0);
		
		// Set up session domain
		if (empty($domain))
		{
			$domain = $this->GetServerVariable('SERVER_NAME', 'localhost');
		}
		
		// Use HTTPS?
		$secure = $this->core->Configuration()->GetBool('session_secure', TRUE);
		
		// Set the session save handler from configuration
		$handler = $this->core->Configuration()->GetString('session_handler', 'files');
		ini_set('session.save_handler', $handler);
		
		// With default file handler, check if the path is writable
		$save_path = $this->core->Configuration()->GetString('session_path', '/tmp');
		if ($handler === 'files')
		{
			if (!is_dir($save_path) || !is_writable($save_path))
			{
				throw new RuntimeException(
					sprintf(
						_('%s "%s" was not found, was not directory or is not writable'),
						_('Session storage path'),
						$save_path
					)
				);
			}
		}
		
		// Set the session save path
		session_save_path($save_path);
		
		// Start the session
		session_set_cookie_params($limit, $path, $domain, $secure);
		session_start();
		
		// Validate session
		if (!$this->IsValid())
		{
			$this->Regenerate(TRUE);
		}
		
		// Update expiry
		$this->SetInt(self::EXPIRY_KEY, time() + $this->timeout);
	}
	
	/**
	 * Get the module name
	 *
	 * @return string
	 */
	public function GetName() : string
	{
		return self::MODULE_NAME;
	}
	
	/**
	 * Close the current session and destroy its data
	 */
	public function Close() : void
	{
		// Regenerate session safely
		$this->Regenerate();
		
		// And destroy session
		session_destroy();
	}
	
	/**
	 * Get the current session ID as seen by PHP
	 *
	 * @return string
	 *    The session ID
	 */
	public function GetSID() : string
	{
		return session_id();
	}
	
	/**
	 * Write the current session out and "pause" it
	 * to avoid long call blocking the session
	 */
	public function Pause() : void
	{
		session_write_close();
	}
	
	/**
	 * Resume the paused session
	 */
	public function Resume() : void
	{
		session_start();
	}
	
	/**
	 * Regenerate the session ID to prevent session hijacking attempts, for
	 * example after the user login.
	 *
	 * If the session is cleared, it will clean up all the session data,
	 * otherwise it will only regenerate the ID.
	 *
	 * @param bool $clear
	 *    If TRUE, will delete all the session specific data.
	 *
	 * @return bool
	 *    Return FALSE if session is obsolete, TRUE otherwise
	 */
	public function Regenerate(bool $clear = FALSE) : bool
	{
		$obsolete = $this->GetBool(self::OBSOLETE_KEY, FALSE);
		
		// Make sure the session is not obsoleted yet
		if (!$clear && $obsolete)
		{
			return FALSE;
		}
		
		// Set the current session to obsolete, if it is not yet
		if (!$obsolete)
		{
			// Set up expiration for slow AJAX and like
			$this->SetBool(self::OBSOLETE_KEY, TRUE);
			
			// Make sure we don't update expiry on already obsolete session
			$this->SetInt(self::EXPIRY_KEY, time() + $this->obsolete_timeout);
		}
		
		
		// If session was cleared out, we need to reinitialize its variables
		if ($clear)
		{
			// Unset everything
			session_regenerate_id(TRUE);
			
			// Make sure session is cleared
			$_SESSION = [];
			
			// Set up current IP and Agent
			$this->SetString(self::REMOTE_ADDRESS_KEY, md5($this->GetServerVariable('REMOTE_ADDR', 'localhost')));
			$this->SetString(self::USER_AGENT_KEY, md5($this->GetServerVariable('HTTP_USER_AGENT', 'Unknown HTTP User Agent')));
			
			// Set expiry
			$this->SetInt(self::EXPIRY_KEY, time() + $this->timeout);
			
			// We have generated the SID
			$this->SetBool(self::SID_KEY, TRUE);
			
			return TRUE;
		}
		
		// Regenerate session and preserve data
		session_regenerate_id();
		
		// Remove the possible session obsoleting from the new session
		unset($_SESSION[self::OBSOLETE_KEY]);
		
		// And update expiry
		$this->SetInt(self::EXPIRY_KEY, time() + $this->timeout);
		
		return TRUE;
	}
	
	/**
	 * Check if the key exists
	 *
	 * @param string $key
	 *    The key to check
	 *
	 * @return bool
	 *    TRUE if the key exists, FALSE otherwise
	 */
	public function Has(string $key) : bool
	{
		return isset($_SESSION[$key]);
	}
	
	/**
	 * Get the session variable and return its value, or default if the value
	 * is not set
	 *
	 * @param string $key
	 *    The key to retrieve
	 *
	 * @param null $default
	 *    The default value for the key, NULL to throw error if key is not set
	 *
	 * @return mixed
	 *    The value of the key
	 *
	 * @throws MissingValue
	 *    If the key is not set and no default value is specified
	 *
	 */
	public function Get(string $key, $default = NULL) : mixed
	{
		if (isset($_SESSION[$key]))
		{
			return $_SESSION[$key];
		}
		
		if (!is_null($default))
		{
			return $default;
		}
		
		throw new MissingValue($key);
	}
	
	/**
	 * Set the session variable
	 *
	 * @param string $key
	 *    The name of the key to write
	 *
	 * @param mixed $value
	 *    The value to write
	 */
	public function Set(string $key, $value) : void
	{
		$_SESSION[$key] = $value;
	}
	
	/**
	 * Get a PHP server variable, or default value if it is not set
	 *
	 * @param string $key
	 *    The key to read from
	 *
	 * @param string $default
	 *    The default value to return if the key is not set
	 *
	 * @return string
	 *    Either the value from the server variables, or default if the key is not set
	 */
	protected function GetServerVariable(string $key, string $default) : string
	{
		if (!isset($_SERVER[$key]))
		{
			return $default;
		}
		
		return (string)$_SERVER[$key];
	}
	
	/**
	 * Make sure session ID, remote address, user agent etc. are
	 * still same and time limit has not expired.
	 *
	 * @return bool
	 *    TRUE if session is still valid, FALSE otherwise
	 */
	protected function IsValid() : bool
	{
		// Session is empty, no point checking more...
		if (empty($_SESSION))
		{
			return FALSE;
		}
		
		// Check for required variables
		if (!isset($_SESSION[self::SID_KEY], $_SESSION[self::REMOTE_ADDRESS_KEY], $_SESSION[self::USER_AGENT_KEY], $_SESSION[self::EXPIRY_KEY]))
		{
			return FALSE;
		}
		
		// Validate the remote address
		if ($this->GetString(self::REMOTE_ADDRESS_KEY) !== md5($this->GetServerVariable('REMOTE_ADDR', 'localhost')))
		{
			return FALSE;
		}
		
		// Validate the user agent
		if ($this->GetString(self::USER_AGENT_KEY) !== md5($this->GetServerVariable('HTTP_USER_AGENT', 'Unknown HTTP User Agent')))
		{
			return FALSE;
		}
		
		// Check for expiry
		if (time() > $this->GetInt(self::EXPIRY_KEY))
		{
			return FALSE;
		}
		
		// Check for Server Generated SID
		if (!$this->GetBool(self::SID_KEY))
		{
			return FALSE;
		}
		
		// So far good
		return TRUE;
	}
}