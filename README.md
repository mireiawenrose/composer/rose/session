# Rose Framework Session
Session support plugin.

## Requirements
- PHP 8.3
- ext-gettext

## Features
Adds the PHP session support to the Rose Framework.
